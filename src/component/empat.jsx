import React, {Component} from 'react';
import styles from '../asset/empat.module.css';
class Empat extends React.Component {
    render(){
        return (
            <body>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Task 3</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Features</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                </li>
                </ul>
            </div>
            </nav>
        <header className={styles.header}>
            <h1>Header.com</h1></header>
        <main className={styles.main}>
            <p>Philip Kotler mendefinisikan brand image sebagai 
                seperangkat keyakinan, ide dan kesan yang dimiliki oleh 
                seseorang terhadap suatu merek. Dan brand image merupakan syarat 
                dari merek yang kuat dan citra adalah persepsi yang relatif konsisten 
                dalam jangka panjang. Dengan adanya brand image yang kuat, maka konsumen 
                akan memiliki keyakinan dan persepsi yang melekat terhadap suatu merek. 
                Dan merek tersebut akan melekat dalam ingatan konsumen (positioning). 
                Sehingga konsumen memiliki ikatan yang kuat dan percaya dengan suatu merek.
            </p>
        </main>
        <footer className={styles.footer}>sumber data dari googling</footer>
        </body>
        )
 };   
};
export default Empat;