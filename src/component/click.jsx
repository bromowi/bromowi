import React, { useState } from 'react';
import '../asset/click.module.css'
class Click extends React.Component {
    constructor(props) {
        super(props);
        this.state = {LogIn: true};
        this.handleClick = this.handleClick.bind(this);
      }
    
      handleClick() {
        this.setState(state => ({
          LogIn: !state.LogIn
        }));
      }
    
      render() {
        return (
          
          <div>
            <h1> {this.state.LogIn ? 'Selamat Datang' : 'Sampai Berjumpa Kembali'}</h1>
            <button onClick={this.handleClick}>
                     Click
            </button>
          </div>
          
        );
      }
    }
              
export default Click

