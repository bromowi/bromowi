import style from '../asset/todolist.module.css'
import { dataJson } from "./todolist/json"
import React, { useState, useEffect} from 'react'
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const Todolist = () =>{

const [todo, setTodo] = useState("") //pengganti setState
const [index, setIndex] = useState(0) //pengganti setState

const removeKey = async (e) => {
    const newTodos = await arr.filter((item) => item !== arr[e])
    setArr(newTodos)
}
const [arr, setArr] = useState([])
useEffect(() => {
    console.log("useeffect")
    if (arr.length <= 0) {
      console.log("data")
      console.log(dataJson, " ")
      console.log(JSON.parse(dataJson), " ")
      console.log("data tutup")
      setArr(JSON.parse(dataJson))
    }
  }, [])

  const submit = (e = null) => {

    setArr((prev) => [...arr, { name: todo, status: false }])
    setIndex(index + 1)
    console.log(arr)
}

const handleChange = (e, x = null) => {
    const { name, value } = e.target
    setTodo(value)

}

return (
    <body>
        <div>
        <div className={style.parent}>
        <div className={style.child}>
    <h1>To Do List</h1>
        <p>Submit Your To Do List</p>
        <Input name="todoText"
                onChange={(e) => {
                  handleChange(e)
                }}
                placeholder="Todo Text"
                value={todo}
                className={style.form}type="textarea" name="text" id="exampleText"
         placeholder="Apa Kegiatanmu hari Ini?"/>
        <div className={style.tombol}>
        <Button className={style.buttons} onClick={() => submit()}> Submit </Button>  
        
        </div> 
        <div>
            
        </div>
        <ul> <br/>
        {Object.keys(arr).map((keyname, i) => (
                <div>
                <li><Label check>
                <Input 
                className={style.kotak}type="checkbox" /> {arr[keyname]["name"]}
                <Button className={style.remove} onClick={() => removeKey(i)}> - </Button>
                </Label></li>
            </div>
        ))
    }
         </ul>
    </div>
    </div>
    </div>
</body>
)
}
export default Todolist