import logo from './logo.svg';
import React, { useState } from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import './App.css';
import { Toast, ToastBody, ToastHeader } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Spinner, Container, Row, Col } from "reactstrap";
import Satu from "./component/satu";
import Todolist from "./component/todolist";
import Dua from "./component/dua";
import Tiga from "./component/tiga";
import Empat from './component/empat';
import Lima from './component/lima';
import Enam from './component/enam';
import Tujuh from './component/tujuh';
import Delapan from './component/delapan';
import Sembilan from './component/sembilan';
import Sepuluh from './component/sepuluh';
import Click from './component/click';
const App = (props) => {
  const [ColapseTodolist, setColapseTodolist] = useState(false);
  const toggleTodolist = () => setColapseTodolist(!ColapseTodolist);

  const [ColapseClick, setColapseClick] = useState(false);
  const toggleClick = () => setColapseClick(!ColapseClick);

  const [Colapse1line, setColapse1line] = useState(false);
  const toggle1line = () => setColapse1line(!Colapse1line);

  const [ColapseSatu, setColapseSatu] = useState(false);
  const toggleSatu = () => setColapseSatu(!ColapseSatu);

  const [ColapseDua, setColapseDua] = useState(false);
  const toggleDua = () => setColapseDua(!ColapseDua);

  const [ColapseTiga, setColapseTiga] = useState(false);
  const toggleTiga = () => setColapseTiga(!ColapseTiga);

  const [ColapseEmpat, setColapseEmpat] = useState(false);
  const toggleEmpat = () => setColapseEmpat(!ColapseEmpat);

  const [ColapseLima, setColapseLima] = useState(false);
  const toggleLima = () => setColapseLima(!ColapseLima);

  const [ColapseEnam, setColapseEnam] = useState(false);
  const toggleEnam = () => setColapseEnam(!ColapseEnam);

  const [ColapseTujuh, setColapseTujuh] = useState(false);
  const toggleTujuh = () => setColapseTujuh(!ColapseTujuh);

  const [ColapseDelapan, setColapseDelapan] = useState(false);
  const toggleDelapan = () => setColapseDelapan(!ColapseDelapan);

  const [ColapseSembilan, setColapseSembilan] = useState(false);
  const toggleSembilan = () => setColapseSembilan(!ColapseSembilan);

  const [ColapseSepuluh, setColapseSepuluh] = useState(false);
  const toggleSepuluh = () => setColapseSepuluh(!ColapseSepuluh);
  return (
    <>
    <div>
      <div className="p-3 bg-dark my-2 rounded">
        <Toast>
          Kumpulan Tugas React Bromo
        </Toast>
    </div>
              {/* Tugas Click */}
                <Click/>
                <br/>
        

          <Button outline color="secondary" onClick={toggleTodolist} 
              style={{ marginBottom: '1rem' }}>
                Tugas To Do List
              </Button>
            <Collapse isOpen={ColapseTodolist}>
                <Todolist/>
            </Collapse>
          </div>

          <div>
          <Button color="primary" onClick={toggle1line} 
              style={{ marginBottom: '1rem' }}>
                Tugas 1 Line Layouts
              </Button>
            <Collapse isOpen={Colapse1line}>

          <div>
            <Button color="primary" onClick={toggleSatu} 
              style={{ marginBottom: '1rem' }}>
                Tugas 1
            </Button>
            <Collapse isOpen={ColapseSatu}>
              <Satu/>
            </Collapse>
          </div>

          <div>
            <Button color="primary" onClick={toggleDua} 
              style={{ marginBottom: '1rem' }}>
                Tugas 2
            </Button>
            <Collapse isOpen={ColapseDua}>
              <Dua/>
            </Collapse>
          </div>

          <div>
            <Button color="primary" onClick={toggleTiga} 
              style={{ marginBottom: '1rem' }}>
                Tugas 3
            </Button>
            <Collapse isOpen={ColapseTiga}>
              <Tiga/>
          </Collapse>
          </div>

          <div>
            <Button color="primary" onClick={toggleEmpat} 
              style={{ marginBottom: '1rem' }}>
                Tugas 4
            </Button>
            <Collapse isOpen={ColapseEmpat}> 
              <Empat/>
              </Collapse>
            </div>

            <div>
            <Button color="primary" onClick={toggleLima} 
              style={{ marginBottom: '1rem' }}>
                Tugas 5
            </Button>
            <Collapse isOpen={ColapseLima}>
              <Lima/>
            </Collapse>
          </div>

          <div>
            <Button color="primary" onClick={toggleEnam} 
              style={{ marginBottom: '1rem' }}>
                Tugas 6
            </Button>
            <Collapse isOpen={ColapseEnam}>
          <Enam/>
          </Collapse>
          </div>

          <div>
            <Button color="primary" onClick={toggleTujuh} 
              style={{ marginBottom: '1rem' }}>
                Tugas 7
            </Button>
            <Collapse isOpen={ColapseTujuh}>
          <Tujuh/>
          </Collapse>
          </div>

          <div>
            <Button color="primary" onClick={toggleDelapan} 
              style={{ marginBottom: '1rem' }}>
                Tugas 8
            </Button>
            <Collapse isOpen={ColapseDelapan}>
          <Delapan/>
          </Collapse>
          </div>

          <div>
            <Button color="primary" onClick={toggleSembilan} 
              style={{ marginBottom: '1rem' }}>
                Tugas 9
            </Button>
            <Collapse isOpen={ColapseSembilan}>
          <Sembilan/>
          </Collapse>
          </div>

          <div>
            <Button color="primary" onClick={toggleSepuluh} 
              style={{ marginBottom: '1rem' }}>
                Tugas 10
            </Button>
            <Collapse isOpen={ColapseSepuluh}>
          <Sepuluh/>
          </Collapse>
          </div>

          </Collapse>
          </div>

          </>
        );
      }

export default App;
